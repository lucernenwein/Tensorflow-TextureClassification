# Texture classification/segmentation using Tensorflow (work in progress)

I started this project mainly because I wanted to start exploring the world of deep learning. I chose Tensorflow because I feel like it's a good starting point due to its ease of use and widespread usage.

The goal is to use machine learning techniques to segment images given a set of user defined learning regions.

## 1 - Algorithm principle

The algorithm can be divided into these successive operations:
1. Extraction of training images
2. Feeding these images to Tensorflow
3. Division of input image in a set of sub-images
4. Classification of these images by Tensorflow
5. Generation of the final segmented image

### Training images

The inputs here are the image to segment (Figure 1 below) and a labeled regions image (Figure 2). These regions must be chosen so that they represent the different textures to recognize in the input image.

To generate the training set of images, I simply use a sliding window inside each of these regions and extract every possible image given the size of desired sub-images. I then just have to feed these images with corresponding class to the Tensorflow model.

### Tensorflow model

For the moment, I implemented three classic models of neural networks in the Tensorflow part of the program:
1. A linear classifier
2. A 3-layer perceptron
2. A convolutional neural network (2 sucessive convolution/pooling/relu layers, 1 fully connected layer)

I use a standard algorithm for backpropagation (ADAM optimizer) as well as for the cost function (softmax cross entropy).

### Classification of the input image pixels

Once the neural network is trained, the question is how to segment the rest of the input image. To do this, I simply use the same sliding window used in the training step to generate one sub-image per pixel. I then -politely- ask Tensorflow to classify these images and give me the highest probability class of each pixel. To eliminate outliers, I associate a class to a pixel only if its most probable one is at least 3 times more probable as the second most probable.

### Generation of the output segmented image

This is probably the most straightforward part, as all I have to do is to create an image the same size as the input one, and set each pixel to the color of its determined region. A result example is shown figure 3 below.

## 2 - Results

Figures below show a few examples of results of this method. Please note that I did not try to fully optimize any parameter to get these results.

![](Images/nature.jpg)*Figure 1: image to segment*

![](Images/nature_labels.png)*Figure 2: input training regions*

![](Images/nature_result.png)*Figure 3: segmented image using the multilayer perceptron*

![](Images/floor2.png)*Figure 4: image to segment*

![](Images/floor_labels2.png)*Figure 5: input training regions*

![](Images/floor2_result.png)*Figure 6: segmented image using the CNN*

![](Images/wood.jpg)*Figure 7: image to segment*

![](Images/wood_labels.png)*Figure 8: input training regions*

![](Images/wood_result.png)*Figure 9: segmented image using the CNN*