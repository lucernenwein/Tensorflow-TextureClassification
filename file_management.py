import glob
import os


def get_files_in_dir(directory, extension):
    """ Returns all files from directory with input extensions.
    """
    os.chdir(directory)
    files = []
    for ext in extension:
        files.extend([file for file in glob.glob("*.{ext}".format(ext = ext))])
    return files


def get_filename(path):
    """Return file name without extension from path.
    """
    b = os.path.split(path)[1]  # path, *filename*
    f = os.path.splitext(b)[0]  # *file*, ext
    return f
