import tensorflow as tf
import numpy as np
import Tools


@Tools.timeit
def apply_classifier(train_labels, train_imgs, input_imgs, classifier_type, images_shape):
    """ Takes an input training set and images to classify, outputs the
    resulting label when using a linear classifier (no hidden layer).
    Args:
        train_labels: 1D numpy array containing 1 at the position of expected label
        train_imgs: 2D numpy array of flattened training images
        input_imgs: 2D numpy array of flattened sub-images to classify
        classifier_type: 0 for linear or 1 for 3-layer perceptron
        images_shape: shape of the images
    Returns: 
        - list of result label number for each sub-image in input_imgs
        - respective probability of every label for each sub-image
    """
    nb_labels = train_labels.shape[1]
    nb_imgs_train = train_imgs.shape[0]
    nb_pix = train_imgs.shape[1]

    keep_prob = tf.placeholder(tf.float32)

    # Create the linear model
    if(classifier_type == 0): # linear classifier
        x = tf.placeholder(tf.float32, [None, nb_pix])
        W = tf.Variable(tf.random_normal([nb_pix, nb_labels]))
        b = tf.Variable(tf.random_normal([nb_labels]))
        y = tf.matmul(x, W) + b
    elif(classifier_type == 1): # 3 layer perceptron
        nb_hidden_neurons = 100
        x = tf.placeholder(tf.float32, [None, nb_pix])

        W1 = tf.Variable(tf.random_normal([nb_pix, nb_hidden_neurons]))
        b1 = tf.Variable(tf.random_normal([nb_hidden_neurons]))

        W2 = tf.Variable(tf.random_normal([nb_hidden_neurons, nb_labels]))
        b2 = tf.Variable(tf.random_normal([nb_labels]))

        y = tf.matmul(tf.nn.relu(tf.matmul(x, W1) + b1), W2) + b2
    elif(classifier_type == 2): # convolution neural network
        
        # Input images layer
        x = tf.placeholder(tf.float32, [None, nb_pix])
        x_image = tf.reshape(x, [-1, images_shape[0], images_shape[1], 3])
        
        # First convolutional layer
        W_conv1 = weight_variable([5, 5, 3, 32])
        b_conv1 = bias_variable([32])
        
        h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
        h_pool1 = max_pool_2x2(h_conv1)

        # Second convolutional layer
        W_conv2 = weight_variable([5, 5, 32, 64])
        b_conv2 = bias_variable([64])

        h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
        h_pool2 = max_pool_2x2(h_conv2)

        # Densely connected layer
        W_fc1 = weight_variable([7 * 7 * 64, 1024])
        b_fc1 = bias_variable([1024])

        h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

        # Dropout regularization layer
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

        # Readout layer
        W_fc2 = weight_variable([1024, nb_labels])
        b_fc2 = bias_variable([nb_labels])

        y = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

    else:
        return []

    # Define loss and optimizer
    y_ = tf.placeholder(tf.float32, [None, nb_labels]) # input correct class

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    #train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)
    train_step = tf.train.AdamOptimizer(0.005).minimize(cross_entropy)

    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()
    
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # Train
    mini_batch_size = 400
    nb_epochs = 15

    for epoch in range(nb_epochs):
        # Shuffling data for each epoch
        train_imgs_shuffled, train_labels_shuffled = Tools.unison_shuffled_copies(train_imgs, train_labels)
        for i in range(0, nb_imgs_train, mini_batch_size):
            batch_xs, batch_ys = train_imgs_shuffled[i:i+mini_batch_size,:], train_labels_shuffled[i:i+mini_batch_size,:]
            _, accuracy_result = sess.run([train_step, accuracy], feed_dict={x: batch_xs, 
                                                                             y_: batch_ys, 
                                                                             keep_prob: 0.5})
        print("Accuracy at epoch={}/{}: {:.3f}".format(epoch+1, nb_epochs, accuracy_result))

    # Test trained model
    accuracy_result = sess.run(accuracy, feed_dict={x: train_imgs_shuffled,
                                                    y_: train_labels_shuffled,
                                                    keep_prob: 1.0})
    
    # Classifying input images
    logits = tf.nn.softmax(y)
    labels = tf.argmax(y, 1)

    # Allocation of output arrays
    labels_result = np.zeros((len(input_imgs)), 'uint8')
    logits_result = np.zeros((len(input_imgs), nb_labels), 'float')

    # Classifying images per batch (to avoid RAM saturation)
    nb_images_per_iteration = 5000
    print('Sub-images classification...')
    for i in range(0, len(input_imgs), nb_images_per_iteration):
        end_index = min(i+nb_images_per_iteration, len(input_imgs)-1)
        labels_result[i:end_index], logits_result[i:end_index,:] = sess.run([labels, logits], 
                                                                            feed_dict={x: input_imgs[i:end_index],
                                                                                       keep_prob: 1.0})
        print('{:.1f}%'.format(100*i/len(input_imgs)))
        
    print("Accuracy={:.3f}".format(accuracy_result))

    # Returning list of result label for each input sub-image
    return labels_result, logits_result


def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')


def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)


def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)
