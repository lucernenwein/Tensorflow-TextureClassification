import numpy as np
import time
from math import ceil
import cv2


def is_label_in_list(label, list_label):
    """ Checks if a label is in list_label.
    label: numpy.array
    list_label: list of numpy.array
    """
    for l in list_label:
        if((l == label).all()):
            return True
    return False


def get_label_index(label, list_label):
    """ Return the index of label in list_label.
    """
    for i, l in enumerate(list_label):
        if((l == label).all()):
            return i

        
def recursive_len(item):
    """ Returns the number of items in nested lists.
    """
    if type(item) == list:
        return sum(recursive_len(subitem) for subitem in item)
    else:
        return 1


def timeit(method):

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print("{} took {} s to complete.".format(method.__name__, round(te-ts, 2)))
        
        return result
    return timed


def convert_to_Tensorflow(list_imgs):
    """ Converts data to type usable by Tensorflow.
    list_imgs: list of list of images. First level is indexed by label number,
    second level is indexed by sub-image number.
    Returns:
        - tensorflow_labels: list of arrays of size nbLabels, each one 
        contains a 1 at the position of the correct label
        - tensorflow_imgs: nbImgs * imgNbPix matrix
    """
    # Getting number of images and labels
    nb_labels = len(list_imgs)
    nb_imgs = recursive_len(list_imgs)
    nb_pix = list_imgs[0][0].size

    # Initialization of arrays
    tensorflow_labels = np.zeros((nb_imgs, nb_labels), 'uint8')
    tensorflow_imgs = np.zeros((nb_imgs, nb_pix), 'float32')
    
    # Initialization of display percentage
    cur_percentage = 0
    display_percentage = 10 ## indicates how many percent to progress before displaying
    print('Converting images to tensorflow matrix...')

    # Filling arrays
    img_index = 0
    for label, list_img in enumerate(list_imgs):
        for i, img in enumerate(list_img):
            tensorflow_imgs[img_index,:] = np.reshape(img / 255. - 0.5, (1, nb_pix))
            tensorflow_labels[img_index,label] = 1
            img_index += 1

            # Display completion percentage
            if(img_index % int(nb_imgs / display_percentage) == 0):
                cur_percentage += display_percentage
                print('{}%'.format(cur_percentage))

    return tensorflow_labels, tensorflow_imgs


def convert_list_imgs_to_Tensorflow(list_imgs):
    """ Converts a list of images to a 2D numpy array with flattened images.
    """
    nb_imgs = len(list_imgs)
    nb_pix = list_imgs[0].size
    # Initialization of array
    tensorflow_imgs = np.zeros((nb_imgs, nb_pix), 'float32')
    for i, img in enumerate(list_imgs):
        tensorflow_imgs[i,:] = np.reshape(img / 255. - 0.5, (1, nb_pix))

    return tensorflow_imgs


def generate_classified_image(img_shape, subimg_step, list_imgs_data, list_result_label, 
                              list_labels, list_logits):
    """ This function takes the output of the Tensorflow classifier and
    generates the result segmented image.
    Args:
        img_shape: (X,Y) tuple representing shape of processed image
        subimg_step: (sizeX1, sizeY1) tuple of undersampling of input image
        list_imgs_data: tuple of lists (out_list_labels, out_list_pixels) where:
            - out_list_labels[n]: value of n-th label (e.g. (255, 0, 0))
            - out_list_img[n]: list of images of class n
        list_result_label: predicted label value for each sub-image
        list_labels: list of color value of each label
        list_logits: list of classification probability for each label
    Returns:
        result classified image
    """
    # If input image was greyscale, we have to add the third dimension for color
    if(len(img_shape) == 2):
        img_shape = (*img_shape, 3)
        
    # Creating resulting image
    result_img  = np.zeros(img_shape, 'uint8')

    # Probability factor definition (we keep only cases for which
    # the most probable label is probability_factor times higher than the second one
    probability_factor = 3.

    sorted_logits_index = np.argsort(list_logits)

    for i, ((x,y), img) in enumerate(list_imgs_data):
        if(list_logits[i, sorted_logits_index[i,-1]] > probability_factor * 
                                                    list_logits[i, sorted_logits_index[i,-2]]):
            result_img[y:y+subimg_step[1],x:x+subimg_step[0]] = list_labels[list_result_label[i]]
        
    return result_img


def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]


def rotate(img, angle):
    """ Applies a rotation on an OpenCV image.
    """
    cols, rows = img.shape[0], img.shape[1]
    M = cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)
    return cv2.warpAffine(img, M, (cols, rows))


def get_N_rotations(nb, max_value):
    """ Returns a list of non-zero regularly spaced positive and negative
    values between -max_value and +max_value. For example:
    get_N_rotations(6, 15) returns [15.0, -15.0, 10.0, -10.0, 5.0, -5.0]
    get_N_rotations(5, 15) returns [15.0, -15.0, 10.0, -10.0, 5.0]
    Args:
        nb: number of values to return
        max_value: maximum positive value
    Returns: 
        list of values
    """
    if(nb==0):
        return []

    # Getting number of positive values
    nb_positive_values = ceil(nb/2)

    # Getting list of positive and negative angles
    step = max_value / nb_positive_values
    l = []
    for i in range(nb_positive_values):
        l.append(max_value-i*step)
        l.append(-max_value+i*step)
    
    return l[:nb]


def load_image(img_path):
    """ Loads an image using OpenCV.
    """
    notOk = False
    img = None
    try:
        img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
    except:
        notOk = True
    
    if(img is None or notOk):
        print('Impossible to load image {}'.format(img_path))
        return None

    return img
