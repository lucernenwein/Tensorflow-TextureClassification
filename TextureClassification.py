import numpy as np
from os.path import join
import file_management
import Tools
import TensorFlow_classifiers
import cv2
import argparse
from math import ceil


def main():
    
    # Parsing input arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('image_path',
                        help='Path to the image to segment')
    parser.add_argument('image_labels_path', 
                        help='Path to the label image (a black image where different colors define the training regions)')
    parser.add_argument('--wx', 
                        dest='sizeX', default=20, type=int, 
                        help='X size of the sliding window')
    parser.add_argument('--wy', 
                        dest='sizeY', default=20, type=int, 
                        help='Y size of the sliding window')
    parser.add_argument('--sx1', 
                        dest='stepX1', default=1, type=int, 
                        help='X axis sub-sample factor of image to segment')
    parser.add_argument('--sy1', 
                        dest='stepY1', default=1, type=int, 
                        help='Y axis sub-sample factor of image to segment')
    parser.add_argument('--sx2', 
                        dest='stepX2', default=1, type=int, 
                        help='X axis sub-sample factor of labels image')
    parser.add_argument('--sy2', 
                        dest='stepY2', default=1, type=int, 
                        help='Y axis sub-sample factor of labels image')
    parser.add_argument('--extend', 
                        dest='extend', action='store_true',
                        help='Use rotations to extend the training sets so that each class has the same number of images')

    args = parser.parse_args()
    
    # Loading input images
    img = Tools.load_image(args.image_path)
    img_label = Tools.load_image(args.image_labels_path)

    if(img is None or img_label is None):
        return

    # Getting all sub-images to classify
    list_input_data = cut_sub_images(img, args.sizeX, args.sizeY, args.stepX1, args.stepY1)

    # Getting all images on which we do the training
    list_labels, list_training_imgs = get_training_images(img, img_label, args.sizeX, args.sizeY, args.stepX2, args.stepY2)
    
    list_training_imgs2 = None

    if(args.extend):
        print('Extending the training set using rotations')

        # Extending training set by using rotations
        list_training_imgs2 = extend_training_set(list_training_imgs)

    else:
        list_training_imgs2 = list_training_imgs
    
    # Printing statistics of the training images
    print_training_stats(list_labels, list_training_imgs2)

    # Converting data to tensorflow type
    tensorflow_train_labels, tensorflow_train_imgs = Tools.convert_to_Tensorflow(list_training_imgs2)
    tensorflow_input_imgs = Tools.convert_list_imgs_to_Tensorflow([data[1] for data in list_input_data])
    
    # Applying tensorflow classifier
    list_result_labels, list_result_logits = TensorFlow_classifiers.apply_classifier(
        tensorflow_train_labels, tensorflow_train_imgs, tensorflow_input_imgs, 1, (args.sizeY, args.sizeX, 3))

    result_labels_img = Tools.generate_classified_image(img.shape, (args.stepX1, args.stepY1), 
                                                        list_input_data, list_result_labels, 
                                                        list_labels, list_result_logits)
    
    cv2.imwrite('output_labels.png', result_labels_img)


@Tools.timeit
def cut_sub_images(img, sizeX, sizeY, stepX, stepY):
    """ Cuts an image as a set of sub-images of size sizeX*sizeY.
    Args:
        img: image to cut
        sizeX, sizeY: size of the images to 
    Returns: 
        List of tuples like ((x,y), img) where:
        - (x,y) is the coordinate of the upper left corner of the sub-image
        in the original image
        - img is the sub-image
    """
    if(img.ndim == 2):
        nb_rows, nb_cols = img.shape
    elif(img.ndim == 3):
        nb_rows, nb_cols, nb_chann = img.shape

    list_imgs = []
    for j in range(0, nb_rows-sizeY, stepY):
        for i in range(0, nb_cols-sizeX, stepX):
            list_imgs.append(((i,j), img[j:j+sizeY, i:i+sizeX]))

    return list_imgs


@Tools.timeit
def get_training_images(img, labels, sizeX, sizeY, stepX, stepY):
    """ This function cuts imgs in sub-images, only for regions delimited by labels
    image.
    Args:
        img: training image
        labels: image indicating pixels to consider for training
        sizeX, sizeY: size of the sub-images to extract
        stepX, stepY: sampling step of the sub-images to extract
    Returns:
        Return value is a tuple of lists (out_list_labels, out_list_pixels) where:
        - out_list_labels[n]: value of n-th label (e.g. (255, 0, 0))
        - out_list_img[n]: list of images of n-th label
    """
    # If labels has more than 3 channels, keep only first 3
    if(labels.ndim == 3 and labels.shape[2] >= 4):
        labels = labels[:,:,:3]
        nb_channels = 3
    
    # Getting number of channels for greyscale images
    nb_channels = labels.ndim

    # Erosion to keep only areas entirely contained in the labelled regions
    kernel = np.ones((sizeX+1, sizeY+1), np.uint8)
    img_erode = cv2.erode(labels, kernel, anchor=(0, 0), borderValue=0)

    # Initialization of display percentage
    counter = 0
    cur_percentage = 0
    display_percentage = 10 ## indicates how many percent to progress before displaying
    nb_pix = len(range(0, labels.shape[1]-sizeY, stepY)) * len(range(0, labels.shape[0]-sizeX, stepX))
    print('Getting training images...')

    # Looping on image's pixels
    out_list_labels = []
    out_list_img = []
    for j in range(0, labels.shape[1]-sizeY, stepY):
        for i in range(0, labels.shape[0]-sizeX, stepX):

            # Display completion percentage
            counter += 1
            if(counter % int(nb_pix / display_percentage) == 0):
                cur_percentage += display_percentage
                print('{}%'.format(cur_percentage))

            cur_val = img_erode[i,j]

            if(not (cur_val == (0,)*nb_channels).all()):
                # Current pixel is not zero

                if(not Tools.is_label_in_list(cur_val, out_list_labels)):
                    # Value not present yet, we add corresponding tuple to labels list
                    out_list_labels.append(cur_val)
                    out_list_img.append([])
                    out_list_img[-1].append(img[i:i+sizeX,j:j+sizeY])
                else:
                    # Appending current sub-image to images list
                    out_list_img[Tools.get_label_index(cur_val, out_list_labels)].append(img[i:i+sizeX,j:j+sizeY])

    return out_list_labels, out_list_img


@Tools.timeit
def extend_training_set(training_imgs):
    """ Extends training set by using rotations to generate new images for 
    classes with few images. After this operations all classes have the same number of 
    images.
    Args:
        training_imgs: list of images for each class (second result of get_training_images)
    Returns:
        extended training set
    """
    # Getting number of images in each class and smallest multiplicative factor to get
    # as many images as in the biggest one
    nb_img = [len(l) for l in training_imgs]
    max_nb_img = max(nb_img)
    multiplicative_factor = [max_nb_img/nb for nb in nb_img]

    out_training_imgs = []

    # Looping through all classes
    for i, l in enumerate(training_imgs):
        # Getting all rotations to extend current class
        rotations = Tools.get_N_rotations( ceil(multiplicative_factor[i])-1, 15 )
        
        # Getting list of new images for current class
        new_imgs = []
        for rot in rotations:
            for img in l:
                new_imgs.append(Tools.rotate(img, rot))

        out_training_imgs.append(l)
        out_training_imgs[-1].extend(new_imgs[:max_nb_img-nb_img[i]])

    return out_training_imgs


def print_training_stats(list_labels, list_img):
    """ Displays the number of images per label and color value of each label
    in the console.
    """
    print("Training sample statistics: {} images in {} labels.".format(Tools.recursive_len(list_img), len(list_labels)))
    for i, label in enumerate(list_labels):
        print("Label {}: BGR={}, {} images. ".format(i, list_labels[i], len(list_img[i])))


if __name__ == "__main__":
    main()
